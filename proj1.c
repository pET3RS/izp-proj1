/*
 *   Assignment:  First IZP project at VUT FIT
 *        class:  IZP
 *
 *       Author:  Peter Močáry
 *        Login:  xmocar00
 *           
 *     Due Date:  17.11.2019
 *   Last eddit:  07.11.2019
 *
 *  Description:  Searches in given phone book for contacts matching inputed digit sequence 
 */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

// maximal length of single line ( including \0 )
#define MAX_LEN 101

// letter sequences assigned to numbers 0-9 by index, also contains the digit it self
char *char_seq_arr[] = {"0+", "1", "2abc", "3def", "4ghi", "5jkl", "6mno", "7pqrs", "8tuv", "9wxyz"};

/* 
 * get_line function:
 *      descr.: 
 *          Reads single line from stdin.
 *          The line has maximal length - MAX_LEN
 * 
 *      param.:
 *          1.- char *line - storage for the line
 *
 *      returns:
 *         -1 - line overflow
 *          0 - successfuly loaded line
 *          1 - last line in file
 */
int get_line( char *line ){

    int letter; 
    for ( int i = 0; i < MAX_LEN; i+=1 ){
        letter = getchar();
        
        if ( letter == EOF )
            return 1;
        
        if ( letter != '\n' ) 
            line[i]= (char) tolower(letter);
        else {
            line[i] = '\0'; 
            return 0;
        }
    }
    return -1;
}

/*
 * print_all function:
 *      descr.:
 *          Prints out whole phone book. 
 *      
 *      param.:
 *          none
 *     
 *      returns:
 *          nothing
 */
void print_all(void) {
    
    char line[MAX_LEN];
    int line_switch = 0;
    int return_value = 0;

    while ( (return_value = get_line(line)) != 1 ) {
         
        if ( return_value == -1 ){
           fprintf( stderr, "[E]: Contact line overflow\n" );
           return; 
        }
        
        if (line_switch == 0) {
            printf("%s, ", line);
            line_switch = 1;
        }
        else {
            printf("%s\n", line);
            line_switch = 0;
        }
    }
}

/*
 * cut_begining function
 *      descr.:
 *          Overwrites the given string with its part, where the part starts at given index.
 *          Basicaly cuts a substring from begining of the string starting at index 0 and ending at given index,
 *          also cuts out character at the given index!
 *
 *      param.:
 *          1.- char *string - base string that we want to shorten
 *          2.- int index - index of the last letter we want to cut out
 *      
 *      returns
 *          nothing
 *
 */
void cut_begining(char *string, int index){
    
    char letter;
    int string_len = strlen(string);
    int i = 0;
    
    for ( int j = (index + 1); j < string_len; j += 1 ){
        letter = string[j];
        string[i] = letter; 
        i += 1;
    } 
    string[string_len-index-1] = '\0';
}

/*
 * find_char function
 *      descr.:
 *          Searches for a char from a sequence assigned to the given number
 *          by the char_seq_arr
 * 
 *      param.:
 *          1.- char *string - string in which we are searching
 *          2.- int number  - number by which is propper sequence from char_seq_arr
 *                            sellected for the fn to search in
 *
 *      returns:
 *          0 - found a char from char sequence in string
 *          1 - didnt find any char from char sequence in string
 */
int find_char( char *string, int number){
    
    char *char_seq = char_seq_arr[number];
    int char_seq_len = strlen(char_seq);
    int string_len = strlen(string);
    char string_letter;
    
    for ( int i = 0; i < string_len; i += 1 ){
       string_letter = string[i]; 
       for ( int j = 0; j < char_seq_len; j += 1 ){
            if ( string_letter == char_seq[j] ){
                cut_begining(string, i);
                return 0;
            } 
       }
    }
    return 1;
}

/*
 * search_in_name function
 *      descr.: 
 *          Searches in name for characters assigned to numbers from input.
 *          The assignment happens in char_seq_arr.
 *
 *      param.:
 *          1.- char *name - string representing a name from phonebook
 *          2.- char *search_seq - numbers sequence from input - numbers by which
 *                                 the program is supposed to match contacts from phone book
 *          3.- int search_seq_len - length of search_seq
 *      returns:
 *          0 - for each number from search_seq found a character in name 
 *          1 - didnt find char for each number form search_seq in name
 */
int search_in_name( char *name, char *search_seq, int search_seq_len){
    for ( int i = 0; i < search_seq_len; i += 1 ) {
        if (find_char( name, search_seq[i] - '0' ) == 1){
            return 1;
        }
    }
    return 0;    
}

/*
 * search_in_number function
 *      descr.: 
 *          Function that searches in phone number for numbers from input
 * 
 *      param.:
 *          1.- char *phone_number - phone number
 *      `   2.- char *search_seq - searched numbers
 *          3.- int search_seq_len - length of search_seq
 *                                   amount of inputed numbers
 *      returnd:
 *          0 - all numbers were found
 *          1 - at least 1 number wasn't found 
 */
int search_in_number( char *phone_number, char *search_seq, int search_seq_len ){
    
    for ( int i = 0; i < search_seq_len; i+=1 ){    
        //searching for + only if it's the first character of the number
        //otherwise search for zero
        if ( (phone_number[0] == '+') && (search_seq[0] = '0') ){
            phone_number += 1;
        }
        else if ( (phone_number = strchr( phone_number, search_seq[i] )) != NULL ){ 
            phone_number += 1; // Increment, otherwise we'll find target at the same location
        } 
        else 
            return 1;
    }
    return 0;
}

/* check_search_seq function
 *      descr.:
 *          Check if inputted char sequence contains only numbers
 *
 *      param.:
 *          1.- char *search_seq - inputed sequence
 *          2.- int search_seq_len - length of inputed sequence 
 *      
 *      returns
 *          0 - successful - all inputted characters are numbers 
 *          1 - at least one inputted charracted isn't digit
 */
int check_search_seq( char *search_seq, int search_seq_len ){
   for ( int i = 0; i < search_seq_len; i += 1 ){ 
        if ( (search_seq[i] < '0') || (search_seq[i] > '9') ) 
            return 1; 
   }
   return 0;
}

/*
 * print_valid function
 *      descr.:
 *          Prints the contact if it fits conditions of input. 
 *
 *      param.:
 *          1.- bool success_name - stating if search on name was successful
 *          2.- bool success_name - stating if search in number was successful
 *          3.- char *name - the name of contact
 *          4.- char *number - the number of contact
 *      
 *      returns
 *          nothing
 */
int print_valid(bool success_name, bool success_number, char *name, char *number) {
    if (success_name == true || success_number == true){
        printf("%s, %s\n", name, number);
        return 0;
    }
    return 1;
}

int main( int argc, char *argv[] ){ 
    
    if (argc>2) {
        fprintf(stderr, "[E]: Too many arguments\n");
        return 1; 
    }
    
    // no searched numbers - prints whole phone book
    else if (argc==1) {
        print_all();
        return 0;
    }
    
    char *search_seq = argv[1];
    int search_seq_len = strlen(search_seq);
    
    if ( check_search_seq( search_seq, search_seq_len ) == 1){  
        fprintf(stderr, "[E]: invalid input, takes only digits\n");
        return 1;
    }

    char line[MAX_LEN], line_backup[MAX_LEN];
    int line_switch = 0; //what is on line? 0-name 1-phonenumber

    bool success_name = true, success_number = true; 
    int return_value = 0, not_found = 0; 
    
    while ((return_value = get_line(line)) != 1) {
        
        if (return_value == -1){
           fprintf(stderr, "[E]: Contact line overflow\n");
           return 1; 
        } 
 
        // name part
        if ( line_switch == 0 ){
            strcpy( line_backup, line ); //backing up line so it doesnt get overwritten
            success_name = true;  
            //the search
            if ( search_in_name( line, search_seq, search_seq_len ) == 1)
                success_name = false;
            line_switch = 1;
        }

        // number part
        else {
            success_number = true;  
            //the search
            if ( search_in_number( line, search_seq, search_seq_len ) == 1 )
                success_number = false; 
            if ( print_valid(success_name, success_number, line_backup, line) == 0) //output
                not_found = 1;
            line_switch = 0;
        }
    }
    if (not_found == 0)
        printf("Not found\n");
    return 0;
}
