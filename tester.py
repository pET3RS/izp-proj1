import random
from typing import List, Dict, Callable
import argparse

KEYBOARD_TABLE = {
    '0': ['+'],
    '1': [],
    '2': ['a', 'b', 'c'],
    '3': ['d', 'e', 'f'],
    '4': ['g', 'h', 'i'],
    '5': ['j', 'k', 'l'],
    '6': ['m', 'n', 'o'],
    '7': ['p', 'q', 'r', 's'],
    '8': ['t', 'u', 'v'],
    '9': ['w', 'x', 'y', 'z']
        }

def gen_rand_ascii_char() -> str:
    ord_value = random.randrange(0, 127)
    if ord_value >= ord('\n'):
        ord_value += 1
    return chr(ord_value)


def normalize_position(pos: int, other_positions: List[int]) -> int:
    for other_pos in other_positions:
        if pos >= other_pos:
            pos += 1
    return pos


def gen_random_positions_in_range(positions_range: int, count: int) -> List[int]:
    assert count <= positions_range
    rnd_positions = []
    for x in range(count):
        pos = random.randrange(0, positions_range - x)
        pos = normalize_position(pos, rnd_positions)
        rnd_positions.append(pos) 
        rnd_positions = sorted(rnd_positions)
    return rnd_positions


def gen_valid_char_for_digit(digit: str) -> str:
    letter_i = random.randrange(0, len(KEYBOARD_TABLE[digit]))
    letter = KEYBOARD_TABLE[digit][letter_i]
    return letter


def gen_valid_name(pattern: str, length: int):
    valid_chars_positions = gen_random_positions_in_range(length, len(pattern))   
    acc = ""
    valid_pos_index = 0
    for i in range(length):
        if valid_pos_index < len(pattern) and i == valid_chars_positions[valid_pos_index]:
            acc += gen_valid_char_for_digit(pattern[valid_pos_index])
            valid_pos_index += 1
        else: 
            acc += gen_rand_ascii_char()
    return acc


def gen_invalid_name(pattern: str, length: int):
    # TODO: Fix this to return full ascii table instead of letters
    invalid_digits = list(set('023456789') - set(pattern))
    acc = ""
    for i in range(length):
        digit = random.choice(invalid_digits)
        acc += gen_valid_char_for_digit(digit)
    return acc


def gen_valid_number(pattern: str, length: int) -> str:
    valid_positions = gen_random_positions_in_range(length, len(pattern))
    pattern_i = 0
    acc = ""
    for x in range(length):
        if pattern_i < len(pattern) and x == valid_positions[pattern_i]:
            acc += pattern[pattern_i]
            pattern_i += 1
        else:
            acc += random.choice('0123456789')
    return acc


def gen_invalid_number(pattern: str, length: int) -> str:
    invalid_digits = list(set('0123456789') - set(pattern))
    acc = ""
    for x in range(length):
        acc += random.choice(invalid_digits)
    return acc


def gen_output_from_validity_specs_using_fn(
        gen_valid: bool, 
        gen_invalid: bool, 
        fn_valid: Callable[[str, int], str],
        fn_invalid: Callable[[str, int], str],
        pattern: str,
        length: int) -> str:
    
    output = ""
    if gen_valid:
        output += fn_valid(pattern, length) + '\n'
    elif args.generate_invalid_names:
        output += fn_invalid(pattern, length) + '\n'
    else:
        if random.random() > 0.5:
            output += fn_valid(pattern, length) + '\n'
        else:
            output += fn_invalid(pattern, length) + '\n'
    return output


def execute(args):
    if args.randomize_length:
        seq_length = random.randrange(len(args.pattern), args.length + 1)
    else:
        seq_length = args.length

    for i in range(args.count):
        if args.valid_only:
            print(gen_valid_name(args.pattern, seq_length))
            print(gen_valid_number(args.pattern, seq_length))
        elif args.invalid_only:
            print(gen_invalid_name(args.pattern, seq_length))
            print(gen_invalid_number(args.pattern, seq_length))
        else:
            print(gen_output_from_validity_specs_using_fn(
                args.generate_valid_names, 
                args.generate_invalid_names,
                gen_valid_name,
                gen_invalid_name,
                args.pattern,
                seq_length),
                end='')

            print(gen_output_from_validity_specs_using_fn(
                args.generate_valid_phone_numbers, 
                args.generate_invalid_phone_numbers,
                gen_valid_number,
                gen_invalid_number,
                args.pattern,
                seq_length),
                end='')

        if args.randomize_length:
            seq_length = random.randrange(len(args.pattern), args.length + 1)

parse = argparse.ArgumentParser()
parse.add_argument('pattern')
parse.add_argument('-c', '--count', type=int, default=20)
parse.add_argument('-l', '--length', type=int, default=100)
parse.add_argument('-r', '--randomize-length', action='store_true', default=False)
parse.add_argument('-v', '--valid-only', action='store_true', default=False)
parse.add_argument('-i', '--invalid-only', action='store_true', default=False)

parse.add_argument('-p', '--generate-valid-phone-numbers', action='store_true', default=False)
parse.add_argument('-n', '--generate-valid-names', action='store_true', default=False)
parse.add_argument('-P', '--generate-invalid-phone-numbers', action='store_true', default=False)
parse.add_argument('-N', '--generate-invalid-names', action='store_true')

args = parse.parse_args()

execute(args)
